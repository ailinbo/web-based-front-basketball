# 基于Web前端的篮球网站设计

#### 介绍
实现了Web网站的基础布局，简要写了一个Web前端网站

#### 具体功能
文件名|用途
----|----
audios|音乐
css|css代码
images|图片
js|js代码
videos|视频
index.html|主页
introduction.html|篮球赛事介绍页面
join.html|加入我们页面
match.html|比赛集锦页面
star_player.html|名人堂页面
survey.html|问卷调查页面
tips.html|篮球攻略页面
toy.html|手办商城页面